package com.example.memeholder.title

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.memeholder.R
import com.example.memeholder.databinding.FragmentTitleBinding
import java.lang.Exception
import java.util.*

class TitleFragment : Fragment() {

    private lateinit var binding: FragmentTitleBinding

    override fun onCreateView(
        inflater: LayoutInflater , container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View {

        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.title_fragment_name)

        binding = DataBindingUtil.inflate(
            inflater , R.layout.fragment_title , container , false
        )

        binding.memeInput.setOnKeyListener { v , keyCode , event ->

            if (keyCode == KeyEvent.KEYCODE_ENTER) {

                // TODO: find why getActionId is called twice
                v.findNavController().navigateUp()
                try {
                    v.findNavController().navigate(getActionId())
                } catch (e: Exception) {
                    Toast.makeText(
                        activity?.applicationContext , e.message.toString() , Toast.LENGTH_SHORT
                    ).show()
                }

                binding.root.hideKeyboard()
            }
            return@setOnKeyListener false
        }

        return binding.root
    }

    private fun getActionId(): Int {
        Log.d("ghoulFragment" , "getActionId")

        return when (binding.memeInput.text?.toString()?.toLowerCase(Locale.ROOT)) {

            resources.getString(R.string.ghoul) -> {
                Log.d("ghoulFragment" , resources.getString(R.string.ghoul))
                R.id.action_titleFragment_to_ghoulFragment
            }
            resources.getString(R.string.images) -> {
                Log.d("ghoulFragment" , resources.getString(R.string.images))
                R.id.action_titleFragment_to_animeFragment
            }
            // TODO: make return to fragment for wrong choice
            else -> {
                Log.d("ghoulFragment" , "else")
                throw Exception("Wrong meme name")
            }
        }
    }


    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken , 0)
    }
}
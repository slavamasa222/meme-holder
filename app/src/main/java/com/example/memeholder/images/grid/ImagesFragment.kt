package com.example.memeholder.images.grid

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.memeholder.R
import com.example.memeholder.databinding.FragmentImagesBinding
import com.example.memeholder.images.Callback
import com.example.memeholder.images.ImagesAdapter
import com.example.memeholder.images.ImagesViewModel

class ImagesFragment : Fragment() , Callback {

    private lateinit var binding: FragmentImagesBinding
    private val model: ImagesViewModel by activityViewModels()
    private var spanCount = 3

    override fun onCreateView(
        inflater: LayoutInflater , container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View {

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.images_fragment_name)

        binding = DataBindingUtil.inflate(
            inflater , R.layout.fragment_images , container , false
        )

        initRecycle()

        return binding.root
    }


    private fun initRecycle() {

        val recycleView = binding.root.findViewById<RecyclerView>(R.id.images_recycle_view)

        val imagesIdArray = model.getImageArray()

        if (imagesIdArray != null) {

            val imagesResourcesArray = getResourcesNames(imagesIdArray)
            val adapter = ImagesAdapter(imagesIdArray , this)
            recycleView?.adapter = adapter
        }

        recycleView?.layoutManager = GridLayoutManager(this.context , spanCount)

    }

    override fun onImageClicked(view: View , drawableId: Int) {

        model.drawableFullScreenId.value = drawableId
        // TODO: understand why navigateUp() is not needed here, but needed in TitleFragment
        view.findNavController().navigate(R.id.action_imagesFragment_to_pagerFragment)
    }

    private fun getResourcesNames(imagesIdArray: MutableList<Int>): List<String> {

        val imagesResourcesArray: MutableList<String> = mutableListOf()
        for(imageId in imagesIdArray) {

            val imageResource = resources.getResourceName(imageId)
            imagesResourcesArray.add(imageResource)
        }

        return imagesResourcesArray
    }
}
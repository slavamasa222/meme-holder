package com.example.memeholder.images

import android.R.attr.fragment
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target
import com.example.memeholder.R


// TODO: usage of callback (must read about it)
interface Callback {

    fun onImageClicked(view: View , drawableId: Int)
}


class ImagesAdapter(private val mImageArray: List<Int> , private val callback: Callback)
    : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val image: ImageView = view.findViewById(R.id.image)
    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.picture_row_item , parent , false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {

        Glide.with(holder.image.context)
            .load(mImageArray[position])
            .into(holder.image)

        // TODO: figure out why it.adjustViewBounds doesn't work but holder.image.adjustViewBounds
        //  works fine
        holder.image.setOnClickListener {

            callback.onImageClicked(it , mImageArray[position])
        }
    }

    override fun getItemCount() = mImageArray.size
}
package com.example.memeholder.images.pager

import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.example.memeholder.R
import com.example.memeholder.databinding.FragmentFullScreenImageBinding
import com.example.memeholder.images.ImagesViewModel


// TODO: make more user-friendly
// TODO: setDecorFitsSystemWindows UNDERSTAND

class FullScreenImageFragment : Fragment() {

    private lateinit var binding: FragmentFullScreenImageBinding

    private val model: ImagesViewModel by activityViewModels()
    private var isUiVisible = true

    override fun onCreateView(
        inflater: LayoutInflater , container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View {

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.fsimages_fragment_name)

        binding = DataBindingUtil.inflate(
            inflater ,
            R.layout.fragment_full_screen_image ,
            container ,
            false
        )

        return binding.root
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {

        var drawableId: Int = model.NO_ID
        arguments?.takeIf { it.containsKey("drawable") }?.apply {

            drawableId = getInt("drawable")
        }

        Glide.with(binding.imageView.context)
            .load(drawableId)
            .into(binding.imageView)


        binding.imageView.setOnClickListener {

            if(Build.VERSION.SDK_INT == 30) {
                hideAndShowUi30Sdk()
            }
            else {
                hideAndShowUiOtherSdk()
            }
        }
    }

    private fun hideAndShowUi30Sdk() {

//        binding.pagerToolbar.isVisible = !(binding.pagerToolbar.isVisible)

            activity?.window?.setDecorFitsSystemWindows(true)
            val controller: WindowInsetsController? = activity?.window?.insetsController

            if (controller != null) {

                controller.systemBarsBehavior =
                    WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

                if (isUiVisible) {

                    (activity as AppCompatActivity).supportActionBar?.hide()
                    isUiVisible = false
                } else {

                    (activity as AppCompatActivity).supportActionBar?.show()
                    isUiVisible = true
                }
            }



    }

    private fun hideAndShowUiOtherSdk() {

        if(isUiVisible) {

            (activity as AppCompatActivity).supportActionBar?.hide()
            isUiVisible = false
        }
        else {

            (activity as AppCompatActivity).supportActionBar?.show()
            isUiVisible = true
        }
    }
}
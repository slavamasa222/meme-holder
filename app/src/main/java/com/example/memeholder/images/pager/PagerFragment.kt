package com.example.memeholder.images

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.doOnAttach
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.memeholder.R
import com.example.memeholder.images.pager.FullScreenImageFragment

//TODO: add space between images |img1|<->|img2|

class PagerFragment : Fragment() {

    private lateinit var pagerAdapter: PagerAdapter
    private lateinit var viewPager: ViewPager2

    private val model: ImagesViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater , container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View? {

        hideStatusBar()

        /** pager adapter must be implemented here[onCreateView], because if implemented in
         * [onViewCreated] adapter will call [PagerAdapter.createFragment]
         * and fragment with position 0 will be shown before fragment with
         * position [getCurrentItemPosition]
         */
        pagerAdapter = PagerAdapter(this, model.getImageArray()?.toList())

        return inflater.inflate(R.layout.fragment_pager , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {

        viewPager = view.findViewById(R.id.pager)
        viewPager.adapter = pagerAdapter
        viewPager.setCurrentItem(getCurrentItemPosition(), false)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        showStatusBar()
    }

    private fun getCurrentItemPosition(): Int {

        val mImages = model.getImageArray() ?: return -1

        var position = -1
        for(it: Int in mImages) {

            if(it == model.drawableFullScreenId.value)
            {
                position = mImages.indexOf(it)
                break
            }
        }

        return position
    }

    private fun hideStatusBar() {

        if(Build.VERSION.SDK_INT == 30) {
            val controller: WindowInsetsController? = activity?.window?.insetsController
            controller?.hide(
                WindowInsets.Type.statusBars()
            )
        }
        else {
            activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        }
    }

    private fun showStatusBar() {

        if(Build.VERSION.SDK_INT == 30) {

            activity?.window?.setDecorFitsSystemWindows(true)
            val controller: WindowInsetsController? = activity?.window?.insetsController
            controller?.show(
                WindowInsets.Type.statusBars()
            )
        }
        else {
            activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        (activity as AppCompatActivity).supportActionBar?.show()
    }
}

class PagerAdapter(fragment: Fragment, private val mImages: List<Int>?): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {

        return when(mImages) {
            null -> 0
            else -> mImages.size
        }
    }

    override fun createFragment(position: Int): Fragment {

        val fragment = FullScreenImageFragment()
        if(mImages != null) {

            fragment.arguments = Bundle().apply {
                putInt("drawable", mImages[position])
            }
        }
        return fragment
    }


}
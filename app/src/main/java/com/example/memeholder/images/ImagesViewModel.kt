package com.example.memeholder.images

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.memeholder.R


class ImagesViewModel : ViewModel() {

    val NO_ID = -1
    val drawableFullScreenId = MutableLiveData(NO_ID)
    private val mImageArray: MutableLiveData<MutableList<Int>> by lazy {
        MutableLiveData(mutableListOf())
    }

    init {
        initImagesArray()
    }

    private fun initImagesArray() {

        mImageArray.value?.add(R.drawable.img1)
        mImageArray.value?.add(R.drawable.img2)
        mImageArray.value?.add(R.drawable.img3)
        mImageArray.value?.add(R.drawable.img4)
        mImageArray.value?.add(R.drawable.img5)
        mImageArray.value?.add(R.drawable.img6)
        mImageArray.value?.add(R.drawable.img7)
    }

    fun getImageArray(): MutableList<Int>? {

        return mImageArray.value
    }

}
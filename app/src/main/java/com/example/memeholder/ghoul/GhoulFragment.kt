package com.example.memeholder.ghoul

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.memeholder.R
import com.example.memeholder.databinding.FragmentGhoulBinding

class GhoulFragment : Fragment() {

    private lateinit var  binding: FragmentGhoulBinding
    private var VIDEO_GHOUL = "ghoul"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.ghoul_fragment_name)

        binding = DataBindingUtil.inflate<FragmentGhoulBinding>(
            inflater, R.layout.fragment_ghoul, container, false
        )

        val mediaController = MediaController(context)
        mediaController.setMediaPlayer(binding.ghoulVideo)
        binding.ghoulVideo.setMediaController(mediaController)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        initPlayer()
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    private fun initPlayer() {

        val videoUri = Uri.parse(
            "android.resource://" + activity?.packageName +
                    "/raw/" + VIDEO_GHOUL
        )
        binding.ghoulVideo.setVideoURI(videoUri)
        binding.ghoulVideo.start()
    }

    private fun releasePlayer() {
        binding.ghoulVideo.stopPlayback()
    }


}